# Post Migration Checklist

1. Delete the old repos.
1. Ensure you have Go 1.16 or higher installed.
1. Update your environment variables.
   - `export GOPRIVATE="gitlab.com/dish-cloud/sling/hydra/*"`
   - `export GO111MODULE=auto`
   - Reminder that you will have to restart code editors for them to see these changes.
1. Sign into Gitlab.
1. Create an access token.
   - Click your user icon in the upper right hand corner of any gitlab page, select `Preferences` from the drop down.
   - In the left nav, click `Access Tokens`.
   - Give your token a name.
   - Expiry is optional. I recommend against setting it.
   - Select the `api` scope for your token.
   - Click `Create personal access token`.
      - The following page is your only chance to get the token. If you lose your token, you should revoke it from Gitlab and generate a new one.
   - Put the token in `~/.netrc` in the following format:
       ```
       machine gitlab.com login <firstname.lastname> password <token>
       ```
1. Clone or Pull the latest CMW script.
   - Repo: https://gitlab.com/corey.ogburn/cmw
   - `git clone git@gitlab.com:corey.ogburn/cmw.git`
   - Ensure that the scripts `cmw` and `cmw-tmux` in the repo are both on your path.
   - If you already have the script set up, all you should have to do is pull latest.
1. Install script prereqs
   - `brew install consul postgresql tmux git cassandra`
   - Docker recently introduced the `compose` subcommand that this script uses instead of `docker-compose`. Docker is required (version 3.3 or newer), docker-compose is not.
   - Optionally, the `cmw` script can configure tmux to make it a little easier to use. I recommend also running `cmw tmux-config`.
1. Clone all the repos.
   - `cmw clone`
   - OR manually clone the services and go-local-dev into `~/go/src/gitlab.com/dish-cloud/sling/hydra`
1. Build the services.
   - `cmw rebuild`
      - Note that this will clean the code first by reverting to what's tracked remotely. (`git clean -Xdff` will be run in every repo before downloading dependents and building).
      - In the future if you wish to build all without cleaning, run `cmw ./build.sh`
1. Start the services
   - `cmw start-local-qa`
   - `cmw start-local-beta`

## No More Bitbucket
All branches have been migrated but only a repo's master branch has been converted to Go Modules. Because imports include the domain name of the repo, we can't reference older commits or we'll reference out of date code that still points to bitbucket. These repos should continue to contain zero references to bitbucket in imports, scripts, anything.

None of the proto-\* repos have been converted to Go Modules. There's no benefit to making such small repos with zero 3rd party dependencies keep track of those dependencies. You may notice the proto-\* repos will be marked as `incompatible` when referenced in other project's `go.mod` files. This is not a problem, it indicates that the project will not have it's own `go.mod` file and there should be no side effects.

For now, only the mission critical repos are being migrated. ReNo and other projects will continue to exist in Bitbucket and any development should continue to happen there until they are migrated.

## Versioned Imports
Be careful merging for awhile. All feature branches are unchanged from their state on bitbucket. Pay special attention to your imports. We not only need to keep an eye out for accidentally importing bitbucket URLs but we should all have a good understanding with how Go Modules supports packages v2 and higher. In Go Modules, a change in the major version of a project is reserved for breaking, non-backwards compatible changes. To prevent accidental referencing of a new major version, Go Modules includes the major version in the import path e.g. `gitlab.com/dish-cloud/sling/hydra/go-log/v2`. Only packages in v0 or v1 won't have a version specified. We should all be familiar with [Semantic Versioning](https://semver.org/) and version our updates accordingly. If you're unsure what version to import, pick the latest version. The latest version can be determined by checking the tags of the project in question.

If you are manually adding a tag to a repo, the tag should be `vX.Y.Z` and not `X.Y.Z`.

## Modules Changes
`Gopkg.toml` and `Gopkg.lock` are replaced with `go.mod` and `go.sum` files respectively. `go.mod` lists required dependencies and their versions used by the project and can be updated manually or automatically. `go.sum` lists fingerprints of those packages and is kept up to date by the Go toolchain; you should never have to modify it by hand. If you think there's a problem with your `go.sum` file, it's safe to delete and regenerate it. You can add modules and their versions directly to the `go.mod` file or you can just start using the imports and run `go mod tidy`.

When in doubt, `go mod tidy`. It will scan the current project building a list of 3rd party modules and update `go.mod` by deleting, updating, or adding entries as needed. The command will also download any missing dependencies that are being referenced. A few Go subcommands will also do the equivalent of a `go mod tidy` such as before you build or run a test.

Dep managed versions by vendoring. That is no longer required with Go Modules. Running `go mod tidy` will update the cache located in `$HOME/go/pkg/mod` and `$HOME/go/pkg/sumdb`. You can still create a vendor folder if you like using `go mod vendor` and the contents of the vendor folder will take precidence over anything in the caches.

Read about Go Modules: https://go.dev/blog/using-go-modules

## The CMW Script and TMux
David Besen began working on this script before his departure. I've kept it alive and found it to be incredibly useful for managing our many repos. There's a little bit of documentation listed at the top of both `cmw` and `cmw-tmux`.

The CMW script can clone, run, build, and manage all of our microservices and docker containers for a majority of your day to day tasks. Try a few simple commands like `cmw branch` to list what branches each repo is in, or `cmw start-local-beta` to spin up all the docker images, start all the services in tmux, and preload your local consul with all the values for beta. Need to do something to all the repos that the script doesn't already do? Run `cmw <command>` to run `<command>` in every repo, e.g. `cmw ./linter.sh`.

To help run so many microservices, the scripts run the microservices in TMux, the terminal multiplexer. Your terminal will be subdivided into individual terminals each running one of our services. If you ran `cmw tmux-config` then each terminal is numbered and labeled on their top border so you can keep track of which terminal is which service. With the recommended config applied, you can also click the terminals to change your focus. TMux hotkeys are special. In order not to shadow a hotkey being passed to an individual terminal, TMux listens for *chords* such as `ctrl+b, z` meaning the Control key and the B key are pressed at the same time like a normal hotkey, then both are released, then ONLY Z is pressed. This chord **Z**ooms or unzooms the currently focused terminal.

There are several ways to exit TMux. The chord `ctrl+b, &` will first prompt you in the status bar to be sure you want to kill all the terminals and pressing `y` will do it. `ctrl+b, :` will let you type in TMux commands in the status bar similar to VIM, if you type in the `kill-session` command you will kill all the running terminals and close the session. The chord `ctrl+b, d` will **D**etach from the session but keep all the terminals running in the background. You can view active sessions and their IDs with `tmux ls` and reattach to them using `tmux a -t <sessionID>` or, if there's only one session, the shorthand `tmux a` will attach to it. Don't open tmux inside tmux.